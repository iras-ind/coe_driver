/**
 *
 * @file coe_utilities.h
 * @brief FIle with some utility for the management of the Can Over Ethercat protocol
 *
 */
#include <sys/types.h>
#include <sys/stat.h>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>

#include <bond/Status.h>
#include <bond/Constants.h>
#include <bondcpp/bond.h>

#include <XmlRpcException.h>
#include <soem/ethercat.h>
#include <ros/ros.h>
 
#include <realtime_utilities/realtime_utilities.h>
#include <coe_core/coe_sdo.h>
#include <coe_driver/modules/coe_pdo_shared_memory.h>


namespace coe_driver
{



/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

InterProcessPdo::ErrorCode InterProcessPdo::update ( const uint8_t* ibuffer, double time, const size_t& n_bytes )
{
  if( ( ( sdo_assignement_ != ECT_SDO_TXPDOASSIGN ) || RealTimeIPC::isServer( access_mode_ ) )
  &&  ( ( sdo_assignement_ != ECT_SDO_RXPDOASSIGN ) || RealTimeIPC::isClient  ( access_mode_ ) ) )
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Shared Memory Call." );
    return coe_driver::InterProcessPdo::UNCORRECT_CALL;
  }

  return RealTimeIPC::update(ibuffer,time,n_bytes);
}


InterProcessPdo::ErrorCode InterProcessPdo::flush ( uint8_t* obuffer, double* time, double* latency_time, const size_t& n_bytes )
{
  if( ( ( sdo_assignement_ != ECT_SDO_RXPDOASSIGN ) || RealTimeIPC::isServer( access_mode_ ) )
  &&  ( ( sdo_assignement_ != ECT_SDO_TXPDOASSIGN ) || RealTimeIPC::isClient  ( access_mode_ ) ) ) 
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Shared Memory Call." );
    return coe_driver::InterProcessPdo::UNCORRECT_CALL;
  }

  return RealTimeIPC::flush(obuffer,time,latency_time,n_bytes);
}




/****
*
* 
* 
* 
****/
ModuleSharedMemory::ModuleSharedMemory( const ModuleDescriptorPtr& module, double operational_time, const RealTimeIPC::AccessMode& mode )
: identifier_(module->getIdentifier() ), rx_pdo_(module, ECT_SDO_RXPDOASSIGN, operational_time, mode ), tx_pdo_(module, ECT_SDO_TXPDOASSIGN, operational_time, mode )
{
}
  
ModuleSharedMemory::~ModuleSharedMemory()
{
  
}

ModulesSharedMemory::~ModulesSharedMemory( )
{
  ROS_WARN("Destroying the Modules Shared Memory ");
  {
    for( auto e : modules_shm_ )
    {
      e.reset();
    }
  }
}
  
  
void ModulesSharedMemory::clear() 
{ 
  modules_shm_.clear(); 
}

ModulesSharedMemory::iterator ModulesSharedMemory::begin()         
{ 
  return modules_shm_.begin(); 
}

ModulesSharedMemory::iterator ModulesSharedMemory::end()           
{ 
  return modules_shm_.end(); 
}
  
ModulesSharedMemory::const_iterator ModulesSharedMemory::begin()  const  
{ 
  return modules_shm_.begin();
}

ModulesSharedMemory::const_iterator ModulesSharedMemory::end()    const  
{ 
  return modules_shm_.end();
}
  
ModulesSharedMemory::const_iterator ModulesSharedMemory::cbegin() const
{ 
  return modules_shm_.cbegin(); 
}

ModulesSharedMemory::const_iterator ModulesSharedMemory::cend()   const  
{ 
  return modules_shm_.cend(); 
}
    
const ModuleSharedMemoryPtr&  ModulesSharedMemory::operator[]( const std::string& i ) const  
{ 
  auto const & it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( ModuleSharedMemoryPtr m){ return i == (m->identifier_); } );
  if( it == modules_shm_.end() )
  {
    throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
  }
  return *it;
}

ModuleSharedMemoryPtr& ModulesSharedMemory::operator[]( const std::string& i )
{ 
  auto it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( const ModuleSharedMemoryPtr& m){ return i == m->identifier_; } );
  if( it == modules_shm_.end() )
    throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
  return *it;
}

bool ModulesSharedMemory::insert( ModuleSharedMemoryPtr module_shm ) 
{ 
  for( const ModuleSharedMemoryPtr& module : modules_shm_ )
  {
    if( module->identifier_ == module_shm->identifier_ )
    {
      ROS_ERROR("Module already in the list.");
      return false;
    }
  }
  modules_shm_.push_back(module_shm); 
  return true;
}


}
