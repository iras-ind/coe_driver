/**
 *
 * @file coe_utilities.h
 * @brief FIle with some utility for the management of the Can Over Ethercat protocol
 *
 */
#include <sys/types.h>
#include <sys/stat.h>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>

#include <bond/Status.h>
#include <bond/Constants.h>
#include <bondcpp/bond.h>

#include <XmlRpcException.h>
#include <soem/ethercat.h>
#include <ros/ros.h>
 
#include <realtime_utilities/realtime_ipc.h>
#include <coe_core/coe_sdo.h>
#include <coe_driver/modules/coe_pdo_ipc.h>


namespace coe_driver
{



/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

InterProcessPdo::ErrorCode InterProcessPdo::update ( const uint8_t* ibuffer, double time, const size_t& n_bytes )
{
  if( ( ( sdo_assignement_ != ECT_SDO_TXPDOASSIGN ) || !realtime_utilities::RealTimeIPC::isServer( access_mode_ ) )
  &&  ( ( sdo_assignement_ != ECT_SDO_RXPDOASSIGN ) || !realtime_utilities::RealTimeIPC::isClient( access_mode_ ) ) )
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Shared Memory Call. SDO ASSIGNMENT: %s, ACCESS SERVER MODE? %s", sdo_assignement_ == ECT_SDO_TXPDOASSIGN ? "ECT_SDO_TXPDOASSIGN" : "ECT_SDO_RXPDOASSIGN", realtime_utilities::RealTimeIPC::isServer( access_mode_ ) ? "YES" : "NO" );
    return coe_driver::InterProcessPdo::UNCORRECT_CALL;
  }

  return realtime_utilities::RealTimeIPC::update(ibuffer,time,n_bytes);
}


InterProcessPdo::ErrorCode InterProcessPdo::flush ( uint8_t* obuffer, double* time, double* latency_time, const size_t& n_bytes )
{
  if( ( ( sdo_assignement_ != ECT_SDO_RXPDOASSIGN ) || !realtime_utilities::RealTimeIPC::isServer( access_mode_ ) )
  &&  ( ( sdo_assignement_ != ECT_SDO_TXPDOASSIGN ) || !realtime_utilities::RealTimeIPC::isClient( access_mode_ ) ) ) 
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Shared Memory Call. SDO ASSIGNMENT: %s, ACCESS SERVER MODE? %s",  sdo_assignement_ == ECT_SDO_TXPDOASSIGN ? "ECT_SDO_TXPDOASSIGN" : "ECT_SDO_RXPDOASSIGN", realtime_utilities::RealTimeIPC::isServer(access_mode_ ) ? "YES" : "NO" );
    return coe_driver::InterProcessPdo::UNCORRECT_CALL;
  }

  return realtime_utilities::RealTimeIPC::flush(obuffer,time,latency_time,n_bytes);
}




/****
*
* 
* 
* 
****/
ModuleIPC::ModuleIPC( const ModuleDescriptorPtr& module, double operational_time, const realtime_utilities::RealTimeIPC::AccessMode& mode )
: identifier_(module->getIdentifier() ), rx_pdo_(module, ECT_SDO_RXPDOASSIGN, operational_time, mode ), tx_pdo_(module, ECT_SDO_TXPDOASSIGN, operational_time, mode )
{
}

// ModuleIPC::ModuleIPC( const std::string& identifier, double operational_time, int watchdog_decimation, const realtime_utilities::RealTimeIPC::AccessMode& mode )
// : identifier_( identifier ), rx_pdo_(identifier, ECT_SDO_RXPDOASSIGN, operational_time, watchdog_decimation, mode), tx_pdo_(identifier, ECT_SDO_TXPDOASSIGN, operational_time, watchdog_decimation, mode)
// {
// }

ModuleIPC::~ModuleIPC()
{
  
}

ModuleIPC_vector::~ModuleIPC_vector( )
{
  ROS_WARN("Destroying the Modules Shared Memory ");
  {
    for( auto e : modules_shm_ )
    {
      e.reset();
    }
  }
}
  
  
void ModuleIPC_vector::clear() 
{ 
  modules_shm_.clear(); 
}

ModuleIPC_vector::iterator ModuleIPC_vector::begin()         
{ 
  return modules_shm_.begin(); 
}

ModuleIPC_vector::iterator ModuleIPC_vector::end()           
{ 
  return modules_shm_.end(); 
}
  
ModuleIPC_vector::const_iterator ModuleIPC_vector::begin()  const  
{ 
  return modules_shm_.begin();
}

ModuleIPC_vector::const_iterator ModuleIPC_vector::end()    const  
{ 
  return modules_shm_.end();
}
  
ModuleIPC_vector::const_iterator ModuleIPC_vector::cbegin() const
{ 
  return modules_shm_.cbegin(); 
}

ModuleIPC_vector::const_iterator ModuleIPC_vector::cend()   const  
{ 
  return modules_shm_.cend(); 
}
    
const ModuleIPCPtr&  ModuleIPC_vector::operator[]( const std::string& i ) const  
{ 
  auto const & it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( ModuleIPCPtr m){ return i == (m->identifier_); } );
  if( it == modules_shm_.end() )
    throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
  return *it;
}

ModuleIPCPtr& ModuleIPC_vector::operator[]( const std::string& i )
{ 
  auto it = std::find_if( modules_shm_.begin(), modules_shm_.end(), [&i]( const ModuleIPCPtr& m){ return i == m->identifier_; } );
  if( it == modules_shm_.end() )
    throw std::runtime_error( ("Shared memory identifier '"+ i + "' not in the mapped list" ).c_str() );
  return *it;
}

bool ModuleIPC_vector::insert( ModuleIPCPtr module_shm ) 
{ 
  for( const ModuleIPCPtr& module : modules_shm_ )
  {
    if( module->identifier_ == module_shm->identifier_ )
    {
      ROS_ERROR("Module already in the list.");
      return false;
    }
  }
  modules_shm_.push_back(module_shm); 
  return true;
}


}
