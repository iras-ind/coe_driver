

#include <coe_driver/diagnostic/coe_generic_analyzer.h>

namespace coe_driver 
{
  
  PLUGINLIB_EXPORT_CLASS(coe_driver::CoeGenericAnalyzer, diagnostic_aggregator::Analyzer );
  
}