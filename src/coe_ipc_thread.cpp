
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include "coe_master.h"

inline 
bool setprio(int prio, int sched)
{
  struct sched_param param;
  param.sched_priority = prio;
  if (sched_setscheduler(0, sched, &param) < 0)
  {
    ROS_ERROR("sched_setscheduler");
    return false;
  }
  return true;
}

inline 
bool init_thread( size_t stack_size, int prio, int sched, realtime_utilities::period_info*  pinfo, long  period_ns  )
{
  if( !setprio(prio, sched) )
  {
    printf("Error in setprio.");
    return false;
  }

  printf("I am an RT-thread with a stack that does not generate page-faults during use, stacksize=%zu\n", stack_size);

  //<do your RT-thing here>
  if( pinfo != NULL )
  {
    assert( period_ns > 0 );
    timer_periodic_init( pinfo, period_ns ); 
  }
  
  return true;
}


NRT_TASK_RET_TYPE coeIPCThread( void* master )
{
  
  size_t sync_sleep_period = 50;
  CoeMaster* master_ = static_cast<CoeMaster*>( master );
  
  ROS_INFO("[%s%s%s] %sWaiting for COE thread running", BOLDMAGENTA(), "START", RESET(), BOLDYELLOW());
  while (!master_->coe_hard_rt_thread_running_)
  {
    usleep(100);
  }
  
  
  // Shared memory creation
  ROS_INFO("[%s%s%s] %sPrepare the shared memory", BOLDMAGENTA(), "START", RESET(), BOLDYELLOW());
  for( auto const & module : master_->modules_ )
  {
    coe_driver::ModuleIPC::Ptr shm_mem( new coe_driver::ModuleIPC( module, master_->network_->getOperationalTime(), realtime_utilities::RealTimeIPC::SHMEM_SERVER ) );
    master_->module_shmem_.insert( shm_mem );
    std::cout << shm_mem->identifier_ << std::endl;
  }
  ROS_INFO("[%s%s%s] %sPrepare the shared memory", BOLDGREEN(), " DONE", RESET(), BOLDYELLOW());
  
  
  try 
  {
    struct timespec update_time;
    struct timespec timer_cycle_end;
    struct timespec timer_cycle_end_prev; 
    
    size_t period_ns = master_->network_->getOperationalTime() * 1e9;
    realtime_utilities::period_info  pinfo;
    int max_prio = sched_get_priority_max(SCHED_RR);
    if( !init_thread( MY_STACK_SIZE, max_prio, SCHED_RR, &pinfo, period_ns ) )
    {
      ROS_FATAL("Failed in setting thread rt properties. Exit. ");
      std::raise(SIGINT);
      return NRT_TASK_RET;
    }
    
    clock_gettime(CLOCK_MONOTONIC,&(pinfo.next_period));
    pinfo.next_period.tv_nsec =  ( (pinfo.next_period.tv_nsec / 1000000) + 1 ) * 1000000; /* round to nearest ms */
    timer_cycle_end_prev  = pinfo.next_period;
    
    while (master_->state_ == CoeMaster::RUNNING)
    {
      try 
      {
        clock_gettime(CLOCK_MONOTONIC, &(pinfo.next_period) );
        size_t missed_cycles = realtime_utilities::timer_inc_period(&pinfo);
        realtime_utilities::timer_wait_rest_of_period(&(pinfo.next_period));
        
        // master_->rt_mutex_.lock();
        
        clock_gettime(CLOCK_MONOTONIC, &update_time );
        double act_time = realtime_utilities::timer_to_s( &update_time );  
          
        for( auto & module  : master_->modules_ )
        {
          realtime_utilities::RealTimeIPC::ErrorCode errcode;
          
          // The "ec_slave[i].inputs" are the feedback from the devices
          // The feebdack are written in the shared memeory, and then the status of the network_ object is update
          errcode = master_->module_shmem_[ module->getIdentifier() ]->tx_pdo_.update( ec_slave[module->getAddress()].inputs, act_time, ec_slave[module->getAddress()].Ibytes );
          if( errcode ) 
          {
            master_->master_diagnostic_->push_back( "Tx Update Failed. "  + master_->module_shmem_[ module->getIdentifier() ]->tx_pdo_.to_string( errcode ) );
          }
          master_->data_->setTxHardRT( module->getIdentifier(), master_->module_shmem_[ module->getIdentifier() ]->tx_pdo_.isHardRT() );
          master_->data_->setTxBonded( module->getIdentifier(), master_->module_shmem_[ module->getIdentifier() ]->tx_pdo_.isBonded() );
          module->updateInputs( ec_slave[module->getAddress()].inputs, false );
            
          // The "ec_slave[i].outputs" are the command to the devices that someone has written in the sharedmem
          // The command are read from the shared memory, and then the status of the network_ object is update
          double tm; 
          double latency; 
          errcode = master_->module_shmem_[ module->getIdentifier() ]->rx_pdo_.flush( ec_slave[module->getAddress()].outputs, &tm, &latency, ec_slave[module->getAddress()].Obytes );
          if( errcode ) 
          {
            master_->master_diagnostic_->push_back( module->getIdentifier() +" " + master_->module_shmem_[ module->getIdentifier() ]->tx_pdo_.to_string( errcode ) );
          }
          master_->data_->setRxHardRT( module->getIdentifier(), master_->module_shmem_[ module->getIdentifier() ]->rx_pdo_.isHardRT() );
          master_->data_->setRxBonded( module->getIdentifier(), master_->module_shmem_[ module->getIdentifier() ]->rx_pdo_.isBonded() );
          master_->data_->setLatencyTime(latency);
          module->updateOutputs( ec_slave[module->getAddress()].outputs, false );
        }
        master_->sync_ready_ = false;
        master_->sync_processed_ = true;
        
        master_->data_->setMissedCycles ( master_->missed_cycles_ );        
        if( master_->data_->getMeanMissedCycles() > 5 )
        {
            ROS_FATAL("Coe Fatal Error. Decrease of perfromance (%u mean missed, %u max %u min in %fs). Abort.", master_->data_->getMeanMissedCycles(), master_->data_->getMaxMissedCycles(), master_->data_->getMinMissedCycles(), master_->data_->getWindowDim() * master_->data_->getCycleTime()  );
            break;
        }
        //master_->rt_mutex_.unlock();
        
      // -----------------------------------------------------------
          
        
      }
      catch( std::exception& e)
      {
        ROS_WARN("[ IPC ] Exception:\n%s", e.what());
        master_->doClose();
        return NRT_TASK_RET;
      }      
    }
    //----------------------------------------------------------------------------------------

    ROS_INFO("%sRequest init state for all slaves.%s",BOLDCYAN(),RESET());
    ec_slave[0].state = EC_STATE_INIT;
    ec_writestate(0);/* request INIT state for all slaves */
    ec_close();
    
    printf("\n\n\n%s***************** IPC Closed. *****************%s\n\n", BOLDMAGENTA(), RESET());
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s", e.what());
    ROS_ERROR("Abort.");
    usleep( 20000000 );
    return NRT_TASK_RET;
  }
  catch (...)
  {
    ROS_ERROR("Unhandled exception ");
    ROS_ERROR("Abort.");
    return NRT_TASK_RET;
  }
  
  return NRT_TASK_RET;
}

