
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include "coe_master.h"

typedef void * (*THREADFUNCPTR)(void *);

#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
#include <alchemy/task.h>
#include <alchemy/pipe.h>
#endif

CoeMaster::CoeMaster( )
: soem_configured_(false), sync_ready_(false), sync_processed_(false ), coe_hard_rt_thread_running_(false)
{
  
}

CoeMaster::~CoeMaster()
{
  ROS_WARN("Killing the Coe Driver.");
  if( coe_hard_rt_thread_rc_ != 0)
  {
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
    if (rt_task_join(&coe_hard_rt_thread_) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
#else
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
      ROS_ERROR("CLOCK REALTIM FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;

    if (pthread_timedjoin_np(coe_hard_rt_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
#endif
  }
  
  if( coe_ipc_thread_rc_ != 0 )
  {
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
      ROS_ERROR("CLOCK REALTIM FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;

    if (pthread_timedjoin_np(coe_ipc_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
  }
  if( coe_error_thread_rc_ != 0 )
  {
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
      ROS_ERROR("CLOCK REALTIM FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;

    if (pthread_timedjoin_np(coe_error_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
  }
}

bool CoeMaster::setPO2SOcallback( coe_soem_utilities::PO2SOconfigFcn setupFcn )
{
  ROS_INFO("Connect the PO2SO Controller Callback Functions");
  for( auto const i : network_->getAddresses() ) 
  {
    config_fcn_[i] = setupFcn;
  }
  return true;
}


void CoeMaster::doOpen( )
{

  try
  { 
    nh_ = ros::NodeHandle("~");
    network_.reset( new coe_driver::NetworkDescriptor(nh_, COE_DEVICE_PARAMETER_NAMESPACE ) );
    
    if( !network_->initNetworkNames( ) )
    {
      ROS_FATAL("Fail in extracting the coe configuration information from ros param server. Abort.");
      throw std::runtime_error("Fail in extracting the coe configuration information from ros param server. Abort.");
    }
    setStatusMessage("Network configuration successfully.", true);
      
    double diagnostic_period_parameter;
    if( !nh_.getParam( "diagnostic_period_parameter", diagnostic_period_parameter ) )
    {
      ROS_FATAL("diagnostic_period_parameter not in the ROSPARAM SERVER. Abort");
      throw std::runtime_error("Fail in extracting the coe configuration information from ros param server. Abort.");
    }
    
    data_.reset( new MainThreadSharedData( 500 )  );
    data_->setCycleTime( network_->getOperationalTime() );
    setStatusMessage("Diagnostic Configuration successfully.", true);
    
    
    for( auto const & module : network_->getAddressUniqueIdMap() )
    {
      module_diagnostic_[ module.first ].reset( new coe_driver::ModuleDiagnostic( module.first ) );
    }
    
    master_diagnostic_.reset(new coe_driver::MasterDiagnostic() );
    
    setStatusMessage("Device opened successfully.", true);
    state_ = OPENED;
  }
  catch( std::exception& e )
  {
    ROS_FATAL("%s", e.what() );
    doClose();
    setStatusMessagef("Exception thrown while opening Hokuyo.\n%s", e.what());
    return;
  }
}

void CoeMaster::doClose( )
{
  try
  {
    setStatusMessage("Device closed successfully.", true); 
  } 
  catch (std::exception& e)   
  {
    setStatusMessagef("Exception thrown while trying to close:\n%s",e.what());
  }
  state_ = CLOSED; // If we can't close, we are done for anyways.
}

void CoeMaster::doStart( )
{
  try
  {
    
    
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
  ROS_INFO("Prepare HARD-RT thread.");
  coe_hard_rt_thread_rc_ = rt_task_create(&coe_hard_rt_thread_, "coe_rt_master", PTHREAD_STACK_MIN + MY_STACK_SIZE , 99, T_JOINABLE);
  switch( coe_hard_rt_thread_rc_ )
  {
    case -EINVAL: printf("either prio, mode or stksize are invalid"); 
    case -ENOMEM: printf("the system fails to get memory from the main heap in order to create the task"); 
    case -EEXIST: printf("minor is different from P_MINOR_AUTO and is not a valid minor number for the pipe special device either (i.e. /dev/rtp*)."); 
  }
  rt_task_start(&coe_hard_rt_thread_, &coeHardRTThread, (void*)this);
  
  
#else
    ROS_INFO("Prepare SOFT-RT thread.");
    pthread_attr_t coe_hard_rt_thread_attr;
    pthread_attr_init(&coe_hard_rt_thread_attr);
    pthread_attr_setstacksize(&coe_hard_rt_thread_attr, PTHREAD_STACK_MIN + MY_STACK_SIZE );
    pthread_attr_setdetachstate(&coe_hard_rt_thread_attr, PTHREAD_CREATE_JOINABLE);
    
    setStatusMessagef( "Wating for RT Coe Loop Thread" );
    coe_hard_rt_thread_rc_ = pthread_create(&coe_hard_rt_thread_, &coe_hard_rt_thread_attr, &coeHardRTThread, (void*)this );
#endif
    if (coe_hard_rt_thread_rc_!=0)
    {
        ROS_ERROR("ERROR; return code from pthread_create() is %d\n", coe_hard_rt_thread_rc_);
        perror( "CoeMaster doStart() ");
        exit(-1);
    }
    
  
    ROS_INFO("Prepare IPC thread.");
    pthread_attr_t coe_ipc_thread_attr;
    pthread_attr_init(&coe_ipc_thread_attr);
    pthread_attr_setstacksize(&coe_ipc_thread_attr, PTHREAD_STACK_MIN + MY_STACK_SIZE );
    pthread_attr_setdetachstate(&coe_ipc_thread_attr, PTHREAD_CREATE_JOINABLE);
    
    setStatusMessagef( "Wating for IPC Thread" );
    coe_ipc_thread_rc_ = pthread_create(&coe_ipc_thread_, &coe_ipc_thread_attr, &coeIPCThread, (void*)this );
    if (coe_ipc_thread_rc_ != 0)
    {
        ROS_ERROR("ERROR; return code from pthread_create() is %d\n", coe_ipc_thread_rc_ );
        perror( "CoeMaster doStart() ");
        exit(-1);
    }
    
  /*
    ROS_INFO("Prepare COE ERROR thread.");
    pthread_attr_t coe_error_thread_attr;
    pthread_attr_init(&coe_error_thread_attr);
    pthread_attr_setstacksize(&coe_error_thread_attr, PTHREAD_STACK_MIN + MY_STACK_SIZE );
    pthread_attr_setdetachstate(&coe_error_thread_attr, PTHREAD_CREATE_JOINABLE);
    
    setStatusMessagef( "Wating for ERROR Thread" );
    coe_error_thread_rc_ = pthread_create(&coe_error_thread_, &coe_error_thread_attr, &coeErrorThread, (void*)this );
    if (coe_error_thread_rc_ != 0)
    {
        ROS_ERROR("ERROR; return code from pthread_create() is %d\n", coe_error_thread_rc_ );
        perror( "CoeMaster doStart() ");
        exit(-1);
    }
  */

    state_ = RUNNING;
  }
  catch( std::exception& e)
  {
    doClose();
    setStatusMessagef("Exception thrown while starting Hokuyo.\n%s", e.what());
    connect_fail_ = e.what();
    return;
  }
}

void CoeMaster::doStop()
{
  struct timespec ts;
  
  if (state_ != RUNNING) // RUNNING can exit asynchronously.
    return;

  state_ = OPENED;

  if( coe_hard_rt_thread_rc_ != 0)
  {
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
    if (rt_task_join(&coe_hard_rt_thread_) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
#else
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
        ROS_ERROR("CLOCK REALTIM FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;
    if (pthread_timedjoin_np(coe_hard_rt_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_hard_rt_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
#endif

  }

  if( coe_ipc_thread_rc_ != 0)
  {

    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
        ROS_ERROR("CLOCK REALTIM FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;
    if (pthread_timedjoin_np(coe_ipc_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_ipc_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
  }

  if( coe_error_thread_rc_ != 0)
  {
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) 
    {
        ROS_ERROR("CLOCK ERROR FAILED ...This is probably a bad sign.");
    }
    ts.tv_sec += 5;
    if (pthread_timedjoin_np(coe_error_thread_, NULL, &ts) != 0) 
    {
      ROS_ERROR("coe_error_thread_ did not die after two seconds. Pretending that it did. This is probably a bad sign.");
    }
  }

  setStatusMessagef("Stopped.", true);
}




