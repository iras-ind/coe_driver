
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include "coe_master.h"


NRT_TASK_RET_TYPE coeErrorThread( void* master )
{
  
  CoeMaster* master_ = static_cast<CoeMaster*>( master );
  try 
  {

    realtime_utilities::period_info  pinfo;
    int max_prio = sched_get_priority_max(SCHED_RR);
    
    ROS_INFO("[%s%s%s] %sWaiting for COE thread running", BOLDMAGENTA(), "START", RESET(), BOLDYELLOW());
    while (!master_->coe_hard_rt_thread_running_)
    {
      usleep(100);
    }
    
    while (master_->state_ == CoeMaster::RUNNING)
    {
        while(EcatError)
        {
          ROS_WARN("RT Loop - EcatError: adding errors to the Diagnostic queue");
          auto errors = coe_soem_utilities::soem_errors();
          for( ec_errort & error : errors )
          {
            std::cout << coe_soem_utilities::to_string( error ) << std::endl;
            master_->module_diagnostic_[ error.Slave ]->push_back( error );
          }
        }
        usleep(10e3);
    }
    //----------------------------------------------------------------------------------------
    printf("\n\n\n%s***************** ERROR Thread Closed. *****************%s\n\n", BOLDMAGENTA(), RESET());
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s", e.what());
    ROS_ERROR("Abort.");
    usleep( 20000000 );
    return NRT_TASK_RET;
  }
  catch (...)
  {
    ROS_ERROR("Unhandled exception ");
    ROS_ERROR("Abort.");
    return NRT_TASK_RET;
  }
  
  return NRT_TASK_RET;
}




