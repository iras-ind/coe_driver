
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include "coe_master.h"

typedef void * (*THREADFUNCPTR)(void *);

extern std::shared_ptr<CoeMaster> g_master;

int moduleSetup(uint16 slave)
{
  
  // Check Configuration -----------------------------------------------------
  if( !g_master->network_->checkAddress(slave) )
  {
    ROS_WARN("[ %sSetup Slave %d# %s%s ] The slave is among the not mapped nodes", BOLDCYAN(), slave ,RESET(), YELLOW());
    return -1;
  }
  auto const module = g_master->getModule(slave);
  
  std::string msg = ( BOLDBLUE()   + std::string( "Module Setup " ) + RESET() )
                  + std::string("[ " + ( BOLDCYAN()   + std::to_string(slave) +"# " + module->getIdentifier() + RESET() ) + " ]" );
                    
  ROS_INFO_STREAM( "[" << BOLDMAGENTA() << "START" << RESET() << "] " << msg );
  // Check Configuration -----------------------------------------------------
  
  // Check Default Configuration -----------------------------------------------------
  ROS_INFO_STREAM( "[-----] " << msg << " Default configuration? "<< BOLDCYAN() << (module->isDefaultConfig() ? "YES" : "NO" ) << RESET());
  if( module->isDefaultConfig() )
  {
    ROS_INFO_STREAM( "[ " << BOLDGREEN() << "DONE"<< RESET()<<"] "<< RESET() << msg  );
    return 1;
  }
  // Check Default Configuration -----------------------------------------------------

  auto & error_diagnostic = g_master->getCoeDriverTypedErrors();
  
  // PDO Assignement Configuration (through SDO) ------------------------------------------------------
  uint16_t ect_sdo_assign[2] = { ECT_SDO_TXPDOASSIGN, ECT_SDO_RXPDOASSIGN };
  for( size_t i=0; i<2; i++ )
  {
    coe_core::Pdo& pdo = ect_sdo_assign[i] == ECT_SDO_TXPDOASSIGN ? module->getTxPdo() : module->getRxPdo();
    
    size_t  n_pdo_entries  = pdo.nEntries( );               // not grouped. i.e., pdo not grouped. i.e., 1A00:1, 1A00:2, 1A00:3 is one Can Dictionary Object composed by 3 Can Objects 
    std::vector< coe_core::DataObjectPtr > assign_pdo = coe_core::split( pdo );
    size_t  n_pdo_grouped_entries  = assign_pdo.size( );    // grouped. i.e., pdo not grouped. i.e., 1A00:1, 1A00:2, 1A00:3 is one Can Dictionary Object composed by 3 Can Objects 
    ROS_DEBUG_STREAM("[-----] " << msg << (ect_sdo_assign[i] == ECT_SDO_TXPDOASSIGN ? " TxPDO" : " RxPDO") << " Write " << n_pdo_entries << "pdo entries, and "<< n_pdo_grouped_entries  << "grouped entries" << RESET() );
    if( n_pdo_grouped_entries > 0 )
    {
      uint16_t map_1c1x[ n_pdo_grouped_entries + 1 ]; //
      map_1c1x[0] = n_pdo_grouped_entries;
      
      for( size_t j=0; j<n_pdo_grouped_entries; j++ )
      {
        map_1c1x[ j+1 ] = assign_pdo.at(j)->index();
        
      }
      int retval = ec_SDOwrite(slave, ect_sdo_assign[i], 0x00, TRUE, sizeof(map_1c1x), &map_1c1x, EC_TIMEOUTSAFE);

      if( retval < 1 )
      {
        while(EcatError)
        {
          ROS_WARN("EcatError: adding errors to the Diagnostic queue");
          
          auto errors = coe_soem_utilities::soem_errors();
          for( ec_errort & error : errors )
          {
            std::cout << coe_soem_utilities::to_string ( error ) << std::endl;
            error_diagnostic[ error.Slave ]->push_back( error );
          }
        }
        ROS_ERROR_STREAM( "[ " << RED() << " ERROR " << RESET() << " ] " << msg << " Failed in settings the PDOs. See CoE diagnostics for further information." );
      }
    }
  }
  // PDO Assignement Configuration (through SDO) ------------------------------------------------------
  
  
  
  
  // Driver Configuration thorugh SDO ------------------------------------------------------
  {
    const coe_core::Sdo& conf_sdo = module->getConfigurationSdo();
    size_t n_sdo_entries = conf_sdo.nEntries(); 
    
    ROS_INFO_STREAM( "[-----] " << msg << " SDO " << n_sdo_entries << "sdo entries" << RESET() );
    for( auto const & cob : conf_sdo )
    {
      int retval  = -1;
      if( conf_sdo.write_access.at( cob->address() ) )
      {
        ROS_DEBUG_STREAM( "[-----] " << msg << " Write SDO:  " << cob->to_string( ) );  
        retval = ec_SDOwrite(slave, cob->index(), cob->subindex(), FALSE, sizeof(uint16_t) * cob->sizeBytes(),cob->data(), EC_TIMEOUTSAFE);
        ROS_DEBUG_STREAM( "[-----] " << msg << " Write SDO:  retval: " << retval );  
        int dim = sizeof(uint16_t) * cob->sizeBytes();
        uint32_t data;
        retval  = ec_SDOread(slave, cob->index(), cob->subindex(), FALSE, &dim, &data, EC_TIMEOUTSAFE);
        ROS_DEBUG_STREAM( "[-----] " << msg << " Check:  "
                       << "index: "<< coe_core::to_string_hex( cob->index() ) <<":" << coe_core::to_string_hex( cob->subindex() ) << "val: "  << data );  
        
      }
      else
      {
        ROS_DEBUG("%s COB 0x%x, size: %zu, %s", msg.c_str(), cob->index(), cob->sizeBytes( ), (conf_sdo.write_access.at( cob->address() ) ? "WRITE" : "READ" ) );  
        int dim = sizeof(uint16_t) * cob->sizeBytes();
        retval  = ec_SDOread(slave, cob->index(), cob->subindex(), FALSE, &dim, cob->data(), EC_TIMEOUTSAFE);
      }
      if( retval < 1 )
      {
        while(EcatError)
        {
          ROS_WARN("EcatError: adding errors to the Diagnostic queue");
          auto errors = coe_soem_utilities::soem_errors();
          for( ec_errort & error : errors )
          {
            std::cout << coe_soem_utilities::to_string( error ) << std::endl;
            error_diagnostic[ error.Slave ]->push_back( error );
          }
        }
        ROS_ERROR_STREAM( "[ " << RED() << " ERROR " << RESET() << " ]" <<  msg << " Failed in access to SDO. See CoE diagnostics for further information." );
      }
    }
  }
  // SDO Configuration ------------------------------------------------------
  
  
  ROS_INFO_STREAM( "[ " << BOLDGREEN() << "DONE" << RESET() << "] "  << msg );
  return 1;
}
