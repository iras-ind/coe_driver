
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include "coe_master.h"

typedef void * (*THREADFUNCPTR)(void *);
#define MAX_BUFFER_SIZE 1024
#define MAX_SLAVE_NUMBER 16
uint32_t buffer [ MAX_BUFFER_SIZE];
uint32_t ibuffer[ MAX_SLAVE_NUMBER ] [MAX_BUFFER_SIZE];
uint32_t obuffer[ MAX_SLAVE_NUMBER ] [MAX_BUFFER_SIZE];
uint32_t Ibytes [ MAX_SLAVE_NUMBER ];
uint32_t Obytes [ MAX_SLAVE_NUMBER ];

#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
#include <alchemy/task.h>
#include <alchemy/pipe.h>

// static struct traceobj trobj;
#endif

extern std::shared_ptr<CoeMaster> master;


#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
RT_PIPE *pipes;
#else

#endif


RT_TASK_RET_TYPE coeHardRTThread( void* master )
{
  size_t sync_sleep_period;
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
  sync_sleep_period = rt_timer_ns2ticks( 50*1e3 );
#else
  sync_sleep_period = 50;
#endif
  
  
  CoeMaster* master_ = static_cast<CoeMaster*>( master );
  try 
  {

    realtime_utilities::period_info  pinfo;
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
    int max_prio = 99;
#else
    int max_prio = sched_get_priority_max(SCHED_RR);
#endif    
    if( !realtime_utilities::rt_init_thread( MY_STACK_SIZE, max_prio, SCHED_RR, &pinfo, master_->network_->getOperationalTime() * 1e9   ) )
    {
      ROS_FATAL("Failed in setting thread rt properties. Exit. ");
      std::raise(SIGINT);
      return RT_TASK_RET;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    // Bringup the COE MASTER 
    if ( !( master_->soem_configured_ = coe_soem_utilities::soem_init( master_->network_->getAdapterName(), 10.0, master_->network_->getAddressUniqueIdMap() )  ) )
    {
      ROS_FATAL("Fail in initialize the SOEM.");
      std::raise(SIGINT);
      return RT_TASK_RET;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    // Initialization of the struct for the nodes management. Attention: if the nodes does not support the configuration trhough SDO, they will be not fully initialized.
    // In the case, it is necessary to update the nodes information once the IOmap is avaliable
    if( !initNodes( master_->nh_, master_->network_, master_->modules_ ) )
    {
      ROS_FATAL("Fail in extracting the coe configuration information from ros param server. Abort.");
      return RT_TASK_RET;
    }
    
    master_->setPO2SOcallback( moduleSetup );
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    // Get the IOmap and, then configuration of the modules (the transition from PO to SO is when the IOmap is calculated)
    char* IOmap = coe_soem_utilities::soem_config( 10.0, configDc(master_->modules_), configSdoCa(master_->modules_), master_->config_fcn_ );
    if( IOmap == NULL )
    {
      ROS_FATAL("Fail in getting the IOmap. Abort.");
      std::raise(SIGINT);
      return RT_TASK_RET;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    // Get the PDO structure for all the nodes that started with the default config, furthermore, it writes the configuration on the ROSPARAM SERVER
    if( !updateNodes( master_->modules_, IOmap, true ) )
    {
      ROS_FATAL("Fail in extracting the coe configuration information from ros param server. Abort.");
      std::raise(SIGINT);
      return RT_TASK_RET;
    }
    master_->data_->setIOmap( IOmap );
    
  //----------------------------------------------------------------------------------------
    if(!coe_soem_utilities::soem_wait_for_state( EC_STATE_OPERATIONAL ) )
    {
      ROS_ERROR("Arg. Why??");
      std::raise(SIGINT);
      return RT_TASK_RET;
    }        
    printf("\n\n\n%s***************** Ethercat send/receive Loop. *****************%s\n\n", BOLDGREEN(), RESET());
    // ----------------------------------------------
    
    int      wkc = 0;
    struct timespec update_time;
    struct timespec timer_cycle_end;
    struct timespec timer_cycle_end_prev; 
    
    int64_t time_offset = 0;
    
    master_->data_->setExpectedWKC( (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC );

    clock_gettime(CLOCK_MONOTONIC,&(pinfo.next_period));
    pinfo.next_period.tv_nsec =  ( (pinfo.next_period.tv_nsec / 1000000) + 1 ) * 1000000; /* round to nearest ms */
  
    timer_cycle_end_prev  = pinfo.next_period;
    
    size_t expected_wkc = master_->data_->getExpectedWKC();
    size_t period_ns    = master_->data_->getCycleTime()*1e9;
    
    ec_send_processdata();
    
    size_t startup = 0;
    int cnt = 0;
    size_t missed_cycles = 0;
    master_->coe_hard_rt_thread_running_ = true;
    
    while (master_->state_ == CoeMaster::RUNNING)
    {
      try 
      {
        
#if defined(__COBALT__) && !defined(__COBALT__WRAP__)
        int ret = rt_task_wait_period(&missed_cycles);
        switch( ret ) 
        {
          case -EWOULDBLOCK:  printf("is returned if rt_task_set_periodic() was not called for the current task.\n"); break;
          case -EINTR:        printf("rt_task_unblock() was called for the waiting task before the next periodic release point was reached. The overrun counter is also cleared.\n"); break;
          case -ETIMEDOUT:    printf("timer overrun occurred, which indicates that a previous release point was missed by the calling task. \n"); break;
          case -EPERM:        printf("this service was called from an invalid context.\n"); break;
        }
        missed_cycles = startup++ < 10 ? 0 : missed_cycles;
#else
        if( ec_slave[0].hasdc )
        {
          missed_cycles = timer_inc_period(&pinfo, time_offset );
        }
        else
        {
          clock_gettime(CLOCK_MONOTONIC, &(pinfo.next_period) );
          missed_cycles = realtime_utilities::timer_inc_period(&pinfo);
        }
        
        realtime_utilities::timer_wait_rest_of_period(&(pinfo.next_period));
#endif
        
        if( missed_cycles > 10 )
        {
            ROS_FATAL("Coe Fatal Error. %zu Missed Cycles in One Clock. Abort.", missed_cycles );
            break;
        }
        
        
        /// Process the CoE
        //master_->rt_mutex_.lock();
        wkc = ec_receive_processdata(EC_TIMEOUTRET);        
        if( wkc < expected_wkc ) 
        {
          cnt++;
          if( cnt > 5) 
          {
            if( !coe_soem_utilities::soem_reset_to_operational_state ( ) )
            {
              ROS_FATAL("Coe Fatal Error. Communication Interrupted. Abort.");
              break;
            }
            clock_gettime(CLOCK_MONOTONIC, &update_time );
            for( auto & module  : master_->modules_ )
            {
              std::memset( ec_slave[module->getAddress()].outputs, 0x0, ec_slave[module->getAddress()].Obytes );
            }
          }
        }
        else
        {
          cnt = 0;
          clock_gettime(CLOCK_MONOTONIC, &update_time );
        }
        
        if (ec_slave[0].hasdc)
        {
          realtime_utilities::timer_calc_sync_offset(ec_DCtime, period_ns, &time_offset);
        }
        ec_send_processdata();
        //master_->rt_mutex_.unlock();
        
        clock_gettime(CLOCK_MONOTONIC, &timer_cycle_end);
//         master_->data_->setMissedCycles( missed_cycles );
//         master_->data_->setWkc ( wkc );
//         master_->data_->setCalcTime         (  realtime_utilities::timer_difference_s( &timer_cycle_end, &(pinfo.next_period) ) );
//         master_->data_->setActualCycleTime  (  realtime_utilities::timer_difference_s( &timer_cycle_end, &timer_cycle_end_prev ) );
        timer_cycle_end_prev = timer_cycle_end;
      }
      catch( std::exception& e)
      {
        ROS_WARN("[ HARD RT ] Exception thrown while trying to get scan.\n%s", e.what());
        master_->doClose();
        return RT_TASK_RET;
      }      
    }
    //----------------------------------------------------------------------------------------

    ROS_INFO("%sRequest init state for all slaves.%s",BOLDCYAN(),RESET());
    ec_slave[0].state = EC_STATE_INIT;
    ec_writestate(0);/* request INIT state for all slaves */
    ec_close();
    
    printf("\n\n\n%s***************** Ethercat Communication Closed. *****************%s\n\n", BOLDMAGENTA(), RESET());
    master_->state_ = CoeMaster::OPENED;
  }
  catch( std::exception& e )
  {
    ROS_ERROR("%s", e.what());
    ROS_ERROR("Abort.");
    usleep( 20000000 );
    return RT_TASK_RET;
  }
  catch (...)
  {
    ROS_ERROR("Unhandled exception ");
    ROS_ERROR("Abort.");
    return RT_TASK_RET;
  }
  
  return RT_TASK_RET;
}





