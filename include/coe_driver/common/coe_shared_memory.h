
#ifndef __coe__ros__base_shared_memeory__h__
#define __coe__ros__base_shared_memeory__h__

#if defined(COBALT) && defined(ALCHEMY)
#else
#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#endif

#include <tuple>
#include <ros/ros.h>


#include <realtime_utilities/realtime_utilities.h>
#include <coe_core/coe_utilities.h>


namespace coe_driver
{

/**
 * @class RealTimeIPC
 *
 */
#if !defined( MAX_IPC_BUF_LENGHT)
#define MAX_IPC_BUF_LENGHT 1024
#endif

class RealTimeIPC
{
public:
    typedef std::shared_ptr< RealTimeIPC >  Ptr;

    enum Skin       { POSIX, RT_POSIX, RT_ALCHEMY };
    enum AccessMode { PIPE_SERVER, SHMEM_SERVER, MQUEUE_SERVER, PIPE_CLIENT, SHMEM_CLIENT, MQUEUE_CLIENT };
    enum ErrorCode  { NONE_ERROR, UNMACTHED_DATA_DIMENSION, UNCORRECT_CALL, WATCHDOG };
    
    static bool isClient( const RealTimeIPC::AccessMode& mode ) { return (mode == PIPE_CLIENT ) || ( mode == SHMEM_CLIENT) || ( mode == MQUEUE_CLIENT ); }
    static bool isServer( const RealTimeIPC::AccessMode& mode ) { return (mode == PIPE_SERVER ) || ( mode == SHMEM_SERVER) || ( mode == MQUEUE_SERVER ); }
    
    struct DataPacket
    {
      struct Header
      {
        uint8_t bond_flag_;
        uint8_t rt_flag_;
        double  time_;
      } __attribute__((packed)) header_;

      char    buffer[MAX_IPC_BUF_LENGHT];
      DataPacket( ) 
      { 
        clear( ); 
      }
      void clear( ) 
      { 
        header_.bond_flag_ = header_.rt_flag_ = header_.time_ = 0;
        std::memset( &buffer[0], 0x0, MAX_IPC_BUF_LENGHT *sizeof(char) );
      } 
    };


    RealTimeIPC(const std::string& identifier, double operational_time, double watchdog_decimation, const AccessMode& mode, const size_t dim = 0);
    ~RealTimeIPC();

    bool        isHardRT ( );
    bool        setHardRT( );
    bool        setSoftRT( );
                
    bool        isBonded ( );
    bool        bond     ( );
    bool        breakBond( );
    ErrorCode   update   ( const uint8_t* buffer, const double time, const size_t& n_bytes );
    ErrorCode   flush    ( uint8_t* buffer, double* time, double* latency_time, const size_t& n_bytes );

    size_t      getSize( bool prepend_header ) const;
    std::string getName()                      const;
    double      getWatchdog( )                 const;
    std::string to_string( ErrorCode err );

    void        dump( RealTimeIPC::DataPacket* buffer )
    {
        return getDataPacket(buffer);
    }

protected:
  
    bool init( );

    const AccessMode                                  access_mode_;
    Skin                                              rt_skin_;
    const double                                      operational_time_;
    const double                                      watchdog_;

    const std::string                                 name_;
    size_t                                            dim_with_header_;
#if defined(COBALT) && defined(ALCHEMY)
    RT_PIPE                                           rt_pipe_;
#endif
    int                                               rt_pipe_fd_;
    boost::interprocess::mapped_region                shared_map_;
    boost::interprocess::shared_memory_object         shared_memory_;
    std::shared_ptr<boost::interprocess::named_mutex> mutex_;

    double                                            start_watchdog_time_;
    double                                            data_time_prev_;
    double                                            flush_time_prev_;

    size_t                                            bond_cnt_;
    bool                                              bonded_prev_;
    bool                                              is_hard_rt_prev_;

    void getDataPacket( DataPacket* packet );
    void setDataPacket( const DataPacket* packet);
};



/**
 *
 *
 *
 *
 *
 *
 */
inline
RealTimeIPC::RealTimeIPC ( const std::string& identifier, double operational_time, double watchdog_decimation, const AccessMode& mode, const size_t dim )
    : access_mode_         ( mode                 )
    , operational_time_    ( operational_time     )
    , watchdog_            ( watchdog_decimation  )
    , name_                ( identifier  )
    , dim_with_header_     ( dim + sizeof(RealTimeIPC::DataPacket::Header) )  //time and bonding index
    , start_watchdog_time_ ( -1 )
    , data_time_prev_      ( 0  )
    , flush_time_prev_     ( 0  )
    , bond_cnt_            ( 0  )
    , bonded_prev_         ( false )
    , is_hard_rt_prev_     ( false )
{
  if(!init( ) )
  {
    throw std::runtime_error("Error in Init RealTimeIPC. Abort.");
  }
}

inline
bool RealTimeIPC::init( )
{
// RT definitions
#if defined(COBALT)
    bool cobalt_core = true;
#else
    bool cobalt_core = false;
#endif

#if defined(ALCHEMY)
    bool alchemy_skin = true;
#else
    bool alchemy_skin = false;
#endif

    rt_skin_    = !cobalt_core ? POSIX
                : alchemy_skin ? RT_ALCHEMY
                : RT_POSIX;

    // RT definitions
    ROS_INFO ( "[%sSTART%s] %sRealTimeIPC Init%s [ %s%s%s ]", BOLDMAGENTA(), RESET(), BOLDBLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
    if ( dim_with_header_ <= sizeof(RealTimeIPC::DataPacket::Header) )
    {
        ROS_WARN ( "[%sCHECK%s%s] %sRealTimeIPC Init%s [ %s%s%s%s ] Memory does not exist, is it correct?"
                    , BOLDRED(), RESET(), YELLOW(), BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW() );
        return true;
    }

    ROS_DEBUG("[-----] %sRealTimeIPC Init%s [ %s%s%s ] Sizeof uint8 %zu sizeof double %zu sizeof header %zu", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), sizeof(uint8_t), sizeof(double), sizeof(DataPacket::Header));

      
    bool ok = true;
    try
    {
      //------------------------------------------
      boost::interprocess::permissions permissions ( 0677 );
      switch (access_mode_)
      {
      case PIPE_SERVER:
      {
#if defined(COBALT) && defined(ALCHEMY)
        if (rt_pipe_create(&rt_pipe_, name_.c_str(), P_MINOR_AUTO, dim_with_header_) != 0)
        {
            printf("rt_pipe_create error\n");
            ok=false;
        }
#endif
      }
      break;
      case SHMEM_SERVER:
      {
          if ((rt_skin_ == RT_ALCHEMY) || (rt_skin_ == RT_POSIX))
          {
              ROS_ERROR("[MISMATCH] %sRealTimeIPC Init%s [ %s%s%s%s ] Alchemy Skin Found while SHMEM_SERVER is required. Abort.", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW());
              throw std::runtime_error("Alchemy Skin Found while SHMEM_SERVER is required. Abort.");
          }

          if (!boost::interprocess::named_mutex::remove(name_.c_str()))
          {
              ROS_DEBUG("[CHECK] %sRealTimeIPC Init%s [ %s%s%s%s ] Error in Removing Mutex", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW());
          }

          // store old
          mode_t old_umask = umask(0);

          ROS_DEBUG("[-----] %sRealTimeIPC Init%s [ %s%s%s ] Create memory (bytes %s%zu/%zu%s)", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) , dim_with_header_, RESET());
          shared_memory_ = boost::interprocess::shared_memory_object(boost::interprocess::create_only
                            , name_.c_str()
                            , boost::interprocess::read_write
                            , permissions);

          shared_memory_.truncate(dim_with_header_);
          shared_map_ = boost::interprocess::mapped_region(shared_memory_, boost::interprocess::read_write);

          std::memset(shared_map_.get_address(), 0, shared_map_.get_size());

          mutex_.reset(new  boost::interprocess::named_mutex(boost::interprocess::create_only, name_.c_str(), permissions));

          // restore old
          umask(old_umask);
      }
      case MQUEUE_SERVER:
      {
          throw std::runtime_error("Not yet implemented :). Abort.");
      }
      break;
      case PIPE_CLIENT:
      {
          rt_pipe_fd_ = open( ("/proc/xenomai/registry/rtipc/xddp/"+name_).c_str() , O_RDWR);
          if (rt_pipe_fd_ < 0)
          {
              ROS_ERROR("[-----] %sRealTimeIPC Init%s [ %s%s%s%s ] Pipe opening failed.", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW());
              ok = false;

          }
      }
      break;
      case SHMEM_CLIENT:
      {
          ROS_DEBUG("[-----] %sRealTimeIPC Init%s [ %s%s%s ] Bond to Shared Memory (bytes %s%zu/%zu%s)", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), dim_with_header_- sizeof(RealTimeIPC::DataPacket::Header), dim_with_header_, RESET());
          shared_memory_ = boost::interprocess::shared_memory_object(boost::interprocess::open_only
                            , name_.c_str()
                            , boost::interprocess::read_write);

          shared_map_ = boost::interprocess::mapped_region(shared_memory_, boost::interprocess::read_write);
          

          ROS_DEBUG("[-----] %sRealTimeIPC Init%s [ %s%s%s ] Bond to Mutex", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET());
          mutex_.reset(new  boost::interprocess::named_mutex(boost::interprocess::open_only, name_.c_str()));
          
          dim_with_header_ = shared_map_.get_size();

        assert ( dim_with_header_ > sizeof(RealTimeIPC::DataPacket::Header) );

        ROS_DEBUG("[-----] %sRealTimeIPC Init%s [ %s%s%s ] Bond to Shared Memory (bytes %s%zu/%zu%s)", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), dim_with_header_ -  sizeof(RealTimeIPC::DataPacket::Header), dim_with_header_, RESET());
          
        ROS_INFO ("[%sREADY%s] %sRealTimeIPC Init%s [ %s%s%s ] Ready", BOLDGREEN(), RESET(), BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
          
      }
      break;
      case MQUEUE_CLIENT:
      {
          ROS_ERROR("Not yet implemented :). Abort.");
          ok=false;
      }
      break;
      }

      ROS_INFO ( "[%s DONE%s] %sRealTimeIPC Init%s [ %s%s%s ]", BOLDGREEN(), RESET(), BOLDBLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );

    }
    catch ( boost::interprocess::interprocess_exception &e )
    {
      if(( SHMEM_CLIENT == access_mode_ ) && ( e.get_error_code() == boost::interprocess::not_found_error ) )
      {
        ROS_WARN ( "[ %s%s%s ] Memory does not exist, Check if correct?", BOLDCYAN(), name_.c_str(), YELLOW());
        ok = false;
      }
      else
      {
        ROS_ERROR_STREAM ( "In processing module '" << name_ << "' [" << name_ << "] got the error: " << e.what() << " error code: " << e.get_error_code() ) ;
        ok = false;
      }

      ROS_ERROR_STREAM ( "In processing module '" << name_ << "' got the error: " << e.what() ) ;
      ok = false;
    }
    catch ( std::exception& e )
    {
      ROS_ERROR_STREAM ( "In processing module '" << name_ << "' got the error: " << e.what() );
      ok = false;
    }

    return ok;
}



inline
RealTimeIPC::~RealTimeIPC()
{

  if ( dim_with_header_ > sizeof(RealTimeIPC::DataPacket::Header) )
  {
    ROS_INFO ( "[ %s%s%s ][ %sRealTimeIPC Destructor%s ] Shared Mem Destructor", BOLDCYAN(), name_.c_str(), RESET() , BOLDBLUE(), RESET());

    if( isBonded() )
        breakBond();

    try
    {
      switch (access_mode_)
      {
      case PIPE_SERVER:
      {
#if defined(COBALT) && defined(ALCHEMY)
        int ret = rt_pipe_delete(&rt_pipe_);
        if ( ret != 0)
        {
            switch( ret ) 
            {
              case EINVAL: case -EINVAL: printf( "pipe is not a valid pipe descriptor. Abort" ); ok = false; break;
              case EIDRM : case -EIDRM : printf( "is returned if pipe is a closed pipe descriptor. Abort" ); ok = false; break;
              case EPERM : case -EPERM : printf( "this service was called from an asynchronous context. Abort" ); ok = false; break;
            }
        }
#endif
      }
      break;
      case SHMEM_SERVER:
      {
        ROS_INFO ( "[ %s%s%s ][ %sRealTimeIPC Destructor%s ] Remove Shared Mem ", BOLDCYAN(), name_.c_str(), RESET(), BOLDBLUE(), RESET());
        if( ! boost::interprocess::shared_memory_object::remove ( name_.c_str() ) )
        {
          ROS_ERROR ( "Error in removing the shared memory object" );
        }
        ROS_INFO ( "[ %s%s%s ][ %sRealTimeIPC Destructor%s ] Remove Mutex", BOLDCYAN(), name_.c_str(), RESET(), BOLDBLUE(), RESET());

        if ( !boost::interprocess::named_mutex::remove ( name_.c_str() ) )
        {
          ROS_ERROR ( "[ %s%s%s ][ %sRealTimeIPC Destructor%s ] Error", BOLDCYAN(), name_.c_str(), RED(), BOLDBLUE(), RESET());
        }
      }
      case MQUEUE_SERVER:
      {
          throw std::runtime_error("Not yet implemented :). Abort.");
      }
      break;
      case PIPE_CLIENT:
      {
          int ret = close(  rt_pipe_fd_  );
          if ( ret != 00)
          {
              ROS_ERROR("[-----] %sRealTimeIPC Init%s [ %s%s%s%s ] Pipe opening failed.", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW());
          }
      }
      break;
      case SHMEM_CLIENT:
      {
                    
      }
      break;
      case MQUEUE_CLIENT:
      {
          ROS_ERROR("Not yet implemented :). Abort.");
      }
      break;
      }

      ROS_INFO ( "[%s DONE%s] %sRealTimeIPC Init%s [ %s%s%s ]", BOLDGREEN(), RESET(), BOLDBLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
    }
    catch ( boost::interprocess::interprocess_exception &e )
    {
      if( (SHMEM_CLIENT == access_mode_ ) && ( e.get_error_code() == boost::interprocess::not_found_error ) )
      {
        ROS_WARN ( "[ %s%s%s ] Memory does not exist, Check if correct?", BOLDCYAN(), name_.c_str(), YELLOW());
      }
      else
      {
        ROS_ERROR_STREAM ( "In processing module '" << name_ << "' [" << name_ << "] got the error: " << e.what() << " error code: " << e.get_error_code() ) ;
      }

      ROS_ERROR_STREAM ( "In processing module '" << name_ << "' got the error: " << e.what() ) ;
    }
    catch ( std::exception& e )
    {
      ROS_ERROR_STREAM ( "In processing module '" << name_ << "' got the error: " << e.what() );
    }
  }
}

inline
void RealTimeIPC::getDataPacket( RealTimeIPC::DataPacket* shmem )
{
  assert( shmem );
  assert ( shared_map_.get_size() < sizeof(RealTimeIPC::DataPacket) );

  shmem->clear();
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );  // from local buffer to shared memory
  std::memcpy ( shmem, shared_map_.get_address(), shared_map_.get_size() );
  lock.unlock();
}

inline
void RealTimeIPC::setDataPacket( const DataPacket* shmem )
{
    assert( shmem );
    assert ( shared_map_.get_size() < sizeof(RealTimeIPC::DataPacket) );

    boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );
    std::memcpy ( shared_map_.get_address(), shmem, shared_map_.get_size()  );
    lock.unlock();
}


inline
bool RealTimeIPC::isHardRT ( )
{
    if( shared_map_.get_size() == 0 )
        return false;

    DataPacket shmem;
    getDataPacket(&shmem);

    bool is_hard_rt = (shmem.header_.rt_flag_ ==  1);
    if( is_hard_rt_prev_ != is_hard_rt )
    {
        ROS_WARN( "[ %s%s%s ] RT State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN(), name_.c_str(), RESET()
                  , BOLDCYAN(), ( is_hard_rt_prev_ ? "HARD" : "SOFT" ), RESET()
                  , BOLDCYAN(), ( is_hard_rt       ? "HARD" : "SOFT" ), RESET() ) ;
        is_hard_rt_prev_ = is_hard_rt;
    }
    return (shmem.header_.rt_flag_ ==  1);

}


inline
bool RealTimeIPC::setHardRT ( )
{
    if( shared_map_.get_size() == 0 )
        return false;

    ROS_INFO( "[ %s%s%s ] [%sSTART%s] Set Hard RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;

    DataPacket shmem;
    getDataPacket(&shmem);

    if( (shmem.header_.rt_flag_ ==  1) )
    {
        ROS_WARN( "[ %s%s%s%s ] Already hard RT!", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
    }

    shmem.header_.rt_flag_ = 1;
    setDataPacket(&shmem);

    ROS_INFO( "[ %s%s%s ] [%s DONE%s] Set Hard RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
    return true;
}

inline
bool RealTimeIPC::setSoftRT ( )
{
    if( shared_map_.get_size() == 0 )
        return false;

    ROS_INFO( "[ %s%s%s ] [%sSTART%s] Set Soft RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;

    DataPacket shmem;
    getDataPacket(&shmem);

    if( (shmem.header_.rt_flag_ ==  0) )
    {
        ROS_WARN( "[ %s%s%s%s ] Already soft RT!", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
    }

    shmem.header_.rt_flag_ = 0;
    setDataPacket(&shmem);

    ROS_INFO( "[ %s%s%s ] [%s DONE%s] Set soft RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
    return true;
}



inline
bool RealTimeIPC::isBonded ( )
{
    if( shared_map_.get_size() == 0 )
        return false;

    DataPacket shmem;
    getDataPacket(&shmem);


    bool is_bonded = (shmem.header_.bond_flag_ == 1);
    if( bonded_prev_ != is_bonded )
    {
        ROS_WARN( "[ %s%s%s ] Bonding State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN(), name_.c_str(), RESET()
                  , BOLDCYAN(), ( bonded_prev_  ? "BONDED" : "UNBONDED" ), RESET()
                  , BOLDCYAN(), ( is_bonded     ? "BONDED" : "UNBONDED" ), RESET() ) ;
        bonded_prev_ = is_bonded;
    }
    return (shmem.header_.bond_flag_ == 1);
}


inline
bool RealTimeIPC::bond ( )
{
    if( shared_map_.get_size() == 0 )
        return false;

    ROS_INFO( "[ %s%s%s ] [%sSTART%s] Bonding", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;

    DataPacket shmem;
    getDataPacket(&shmem);

    if( (shmem.header_.bond_flag_ ==  1) )
    {
        ROS_ERROR( "[ %s%s%s%s ] Already Bonded! Abort. \n\n****** RESET CMD FOR SAFETTY **** \n", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
        return false;
    }

    shmem.header_.bond_flag_ = 1;
    setDataPacket(&shmem);

    ROS_INFO( "[ %s%s%s ] [%sDONE%s] Bonding", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
    return true;
}

inline
bool RealTimeIPC::breakBond ( )
{
    ROS_INFO( "[ %s%s%s ] Break Bond", BOLDCYAN(), name_.c_str(), RESET() ) ;
    DataPacket shmem;
    getDataPacket(&shmem);

    shmem.header_.rt_flag_ = 0;
    shmem.header_.bond_flag_ = 0;

    setDataPacket(&shmem);

    return true;

}


inline
RealTimeIPC::ErrorCode RealTimeIPC::update ( const uint8_t* ibuffer, double time, const size_t& n_bytes )
{
  if ( ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) != n_bytes )
  {
      ROS_ERROR ( "FATAL ERROR! Shared memory map '%zu' bytes, while the input is of '%zu' bytes", ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) , n_bytes );
      return coe_driver::RealTimeIPC::UNMACTHED_DATA_DIMENSION;
  }

  if ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) <= 0 )
  {
      return coe_driver::RealTimeIPC::NONE_ERROR;
  }

  DataPacket shmem;
  getDataPacket(&shmem);

  if( shmem.header_.bond_flag_ == 1 )
  {
      shmem.header_.time_ = time;
      std::memcpy( shmem.buffer, ibuffer, ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) );
  }

  setDataPacket(&shmem);

  return coe_driver::RealTimeIPC::NONE_ERROR;
}



inline
RealTimeIPC::ErrorCode RealTimeIPC::flush ( uint8_t* obuffer, double* time, double* latency_time, const size_t& n_bytes )
{
    RealTimeIPC::ErrorCode ret = coe_driver::RealTimeIPC::NONE_ERROR;
    if( ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) != n_bytes )
    {
        ROS_ERROR ( "FATAL ERROR! Wrong Memory Dimensions." );
        return coe_driver::RealTimeIPC::UNMACTHED_DATA_DIMENSION;
    }

    if ( ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) <= 0 )
    {
        return coe_driver::RealTimeIPC::NONE_ERROR;
    }

    DataPacket shmem;
    getDataPacket(&shmem);

    if( shmem.header_.bond_flag_ == 1 )
    {
        *time = shmem.header_.time_;
        std::memcpy( obuffer, shmem.buffer, ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) );
    }
    else
    {
        // ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] SAFETTY CMD (not bonded)", BOLDCYAN(), name_.c_str(), RESET(), RED()) ;
        *time = 0.0;
        std::memset ( obuffer, 0x0, ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) );
    }


    // check
    *latency_time = ( *time - data_time_prev_ );

    struct timespec flush_ts;
    clock_gettime( CLOCK_MONOTONIC, &flush_ts);
    double flush_time = realtime_utilities::timer_to_s( &flush_ts );
    if( RealTimeIPC::isClient( access_mode_ ) && ( std::fabs( flush_time - *time  ) > 2 * watchdog_ ) )
    {
        ROS_WARN( "Data not updated! (%f,%f,%f)!", flush_time, *time, watchdog_ );
        return coe_driver::RealTimeIPC::WATCHDOG;
    }

    if( shmem.header_.bond_flag_ == 1 )
    {
        /////////////////////////////////////////////////
        if( ( *latency_time < watchdog_)  && ( *latency_time > 1e-5 ) )  // the client cycle time is in the acceptable trange watchdog
        {
            start_watchdog_time_ = -1;
        }
        else if( *latency_time > watchdog_)
        {
            ROS_WARN( "Latency overcome the watchdog (%f,%f,%f)!", *latency_time, *time, data_time_prev_ );
            ret = coe_driver::RealTimeIPC::WATCHDOG;
        }
        else if ( *latency_time < 1e-5 ) // the client is not writing new data in the shared memory ...
        {
            if( start_watchdog_time_ == -1 )
            {
                start_watchdog_time_ = flush_time;
            }
            else if( ( flush_time - start_watchdog_time_ ) > watchdog_ )
            {
                ret = coe_driver::RealTimeIPC::WATCHDOG;
            }
        }
        /////////////////////////////////////////////////

        /////////////////////////////////////////////////
        if( ret == coe_driver::RealTimeIPC::WATCHDOG )
        {
          if( shmem.header_.rt_flag_==1)
          {
            ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** RESET CMD FOR SAFETTY ****"
                                , BOLDCYAN(), name_.c_str(), RESET(), RED(), ( flush_time - start_watchdog_time_ ), watchdog_ ) ;
            if( RealTimeIPC::isServer(access_mode_) )
            {
                std::memset ( obuffer, 0x0, ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) ) );
            }
          }
          else
          {
            ROS_WARN_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** SOFT RT, DON'T CARE ****"
                                , BOLDCYAN(), name_.c_str(), RESET(), YELLOW(), ( flush_time - start_watchdog_time_ ), watchdog_ ) ;
            ret = coe_driver::RealTimeIPC::NONE_ERROR;
          }
        }
        /////////////////////////////////////////////////
    }
    data_time_prev_ = *time;
    flush_time_prev_ = flush_time;

    return ret;
}





inline std::string RealTimeIPC::to_string( RealTimeIPC::ErrorCode err )
{
    std::string ret = "na";
    switch(err)
    {
    case NONE_ERROR               :
        ret = "SHARED MEMORY NONE ERROR";
        break;
    case UNMACTHED_DATA_DIMENSION :
        ret = "SHARED MEMORY UNMATHCED DATA DIMENSION";
        break;
    case UNCORRECT_CALL           :
        ret = "SHARED MEMORY UNCORRECT CALL SEQUENCE";
        break;
    case WATCHDOG                 :
        ret = "SHARED MEMORY WATCHDOG";
        break;
    }
    return ret;
}

inline
size_t RealTimeIPC::getSize( bool prepend_header ) const
{
    return prepend_header ? dim_with_header_ : ( dim_with_header_ - sizeof(RealTimeIPC::DataPacket::Header) );
}

inline
double RealTimeIPC::getWatchdog(  ) const
{
    return watchdog_;
}

}

#endif
