
#ifndef __coe__ros__pdo__shared_memeory__h__
#define __coe__ros__pdo__shared_memeory__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>

#include <tuple>
#include <ros/ros.h>
#include <bondcpp/bond.h>

#include <coe_core/coe_pdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_driver/modules/coe_network_descriptor.h>

#include <realtime_utilities/realtime_ipc.h>

namespace coe_driver 
{

inline size_t pdoDataPacketDim( const ModuleDescriptorPtr& module, const int sdo_assignement )
{
  size_t dim = (sdo_assignement == ECT_SDO_RXPDOASSIGN ? module->getRxPdo().nBytes ( true ) : module->getTxPdo().nBytes ( true ) );
  return dim;
}

inline std::string pdoIPCIdentifier( const ModuleDescriptorPtr& module, const int sdo_assignement )
{
  return ( sdo_assignement == ECT_SDO_RXPDOASSIGN ? module->getSoemOutputId() : module->getSoemInputId() );
}
inline std::string pdoIPCIdentifier( const std::string& identifier, const int sdo_assignement )
{
  return (identifier + ( sdo_assignement == ECT_SDO_RXPDOASSIGN ? "_rxpdo" : "_txpdo" ));
}





class InterProcessPdo : public realtime_utilities::RealTimeIPC
{
public:
  typedef std::shared_ptr< InterProcessPdo >  Ptr;
  
  InterProcessPdo(const ModuleDescriptorPtr& module, const int sdo_assignement, double operational_time, const realtime_utilities::RealTimeIPC::AccessMode& mode )
  : realtime_utilities::RealTimeIPC ( pdoIPCIdentifier(module, sdo_assignement)
                                    , operational_time
                                    , (double) module->getWatchdogDecimation() * operational_time
                                    , mode
                                    , pdoDataPacketDim( module, sdo_assignement ) )
  , sdo_assignement_ ( sdo_assignement )
  {
  }
//   InterProcessPdo(const std::string& identifier, const int sdo_assignement, double operational_time, int watchdog_decimation, const realtime_utilities::RealTimeIPC::AccessMode& mode )
//   : realtime_utilities::RealTimeIPC ( pdoIPCIdentifier(identifier, sdo_assignement)
//                                     , operational_time
//                                     , watchdog_decimation * operational_time
//                                     , mode )
//   , sdo_assignement_ ( sdo_assignement )
//   {
//   }

  
  ErrorCode update   ( const uint8_t* buffer, const double time, const size_t& n_bytes );
  ErrorCode flush    ( uint8_t* buffer, double* time, double* latency_time, const size_t& n_bytes );
  
private:
  const int sdo_assignement_ ;
  
  
};

typedef InterProcessPdo::Ptr InterProcessPdoPtr;




class ModuleIPC
{
public:
  typedef std::shared_ptr< ModuleIPC > Ptr;
  
  const std::string   identifier_;
  InterProcessPdo     rx_pdo_;
  InterProcessPdo     tx_pdo_;
  
  ModuleIPC( const ModuleDescriptorPtr& module, double operational_time, const realtime_utilities::RealTimeIPC::AccessMode& mode );
//   ModuleIPC( const std::string& identifier, double operational_time, int watchdog_decimation, const realtime_utilities::RealTimeIPC::AccessMode& mode );
  
  ~ModuleIPC();
};

typedef ModuleIPC::Ptr ModuleIPCPtr;


class ModuleIPC_vector
{
public:
  
  typedef std::vector<ModuleIPC::Ptr> List;
  typedef List::iterator              iterator;
  typedef List::const_iterator        const_iterator;
  
  ~ModuleIPC_vector( );
  
  
  void                          clear()       ;
  iterator                      begin()       ;
  iterator                      end()         ;
                                              
  const_iterator                begin()  const;
  const_iterator                end()    const;
                                              
  const_iterator                cbegin() const;
  const_iterator                cend()   const;
    
  const ModuleIPCPtr&  operator[]( const std::string& i ) const;
  ModuleIPCPtr& operator[]( const std::string& i );
  
  bool insert( ModuleIPCPtr module_shm ) ;
  
private:
  
  List modules_shm_;
  
};



}

#endif
