#ifndef __COE_DRIVER__H__
#define __COE_DRIVER__H__

#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>

#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
  #include <alchemy/task.h>
#else
  #include <pthread.h>
#endif

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>

#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include <realtime_utilities/realtime_utilities.h>

#include <coe_core/coe_sdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_soem_utilities/coe_soem_utilities.h>

#include <coe_driver/common/driver_fsm.h>
#include <coe_driver/diagnostic/coe_driver_diagnostics.h>
#include <coe_driver/modules/coe_module_descriptor.h>
#include <coe_driver/modules/coe_network_descriptor.h>
#include <coe_driver/modules/coe_pdo_ipc.h>
#include <coe_driver/modules/coe_srv_utilities.h>


#include "coe_driver_utilities.h"
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
  #define RT_TASK_RET
  #define RT_TASK_RET_TYPE void
#else
  #define RT_TASK_RET nullptr
  #define RT_TASK_RET_TYPE void*
#endif

#define NRT_TASK_RET nullptr
#define NRT_TASK_RET_TYPE void*

static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "coe";

int moduleSetup(uint16 slave);

RT_TASK_RET_TYPE  coeHardRTThread  ( void* );
NRT_TASK_RET_TYPE coeIPCThread     ( void* );
NRT_TASK_RET_TYPE coeErrorThread   ( void* );

class AtomicMutex
{
    std::atomic<bool> flag{false};

public:
    void lock()
    {
        while (flag.exchange(true, std::memory_order_relaxed));
        std::atomic_thread_fence(std::memory_order_acquire);
    }

    void unlock()
    {
        std::atomic_thread_fence(std::memory_order_release);
        flag.store(false, std::memory_order_relaxed);
    }
};

class CoeMaster : public coe_driver::DriverFSM
{
  friend int moduleSetup(uint16 slave);
  friend RT_TASK_RET_TYPE   coeHardRTThread   ( void* );
  friend NRT_TASK_RET_TYPE  coeIPCThread      ( void* );
  friend NRT_TASK_RET_TYPE  coeErrorThread    ( void* );
  ros::NodeHandle                              nh_;
  
  // ---
  bool                                         network_info_;
  coe_driver::NetworkDescriptorPtr             network_;
  std::vector<coe_driver::ModuleDescriptorPtr> modules_;
  coe_driver::ModuleIPC_vector                 module_shmem_;
  // ---
  
  // ---
  std::string                                      connect_fail_;
  coe_driver::MasterDiagnosticPtr                  master_diagnostic_;
  std::map< int, coe_driver::ModuleDiagnosticPtr > module_diagnostic_;
  // --- 
  
  
  std::shared_ptr<MainThreadSharedData> data_;
  bool                                  coe_hard_rt_thread_running_;
  int                                   coe_hard_rt_thread_rc_;
  size_t                                missed_cycles_;
#if defined( __COBALT__ ) &&  !defined( __COBALT_WRAP__ )
  RT_TASK                               coe_hard_rt_thread_;
#else
  pthread_t                             coe_hard_rt_thread_;
#endif
  
  AtomicMutex                           rt_mutex_;
  
  int                                   coe_ipc_thread_rc_;
  pthread_t                             coe_ipc_thread_;

  int                                   coe_error_thread_rc_;
  pthread_t                             coe_error_thread_;
  
  bool                                  soem_configured_;
  bool                                  sync_ready_;
  bool                                  sync_processed_;
  
  std::map< int, coe_soem_utilities::PO2SOconfigFcn>  config_fcn_;

public:
  
  CoeMaster();
  ~CoeMaster();
  
  bool setPO2SOcallback( coe_soem_utilities::PO2SOconfigFcn setupFcn );
  
  void doOpen ( );
  void doClose( );
  void doStart( );
  void doStop ( );
  
  std::string getID();
  
  

  // ACCESSORS
  std::shared_ptr< MainThreadSharedData>                   getData()                                { return data_; }
                                                           
  const std::vector<coe_driver::ModuleDescriptorPtr>&      getModules( ) const;
  std::vector<coe_driver::ModuleDescriptorPtr>&            getModules( );
                                                                                              
  const coe_driver::ModuleDescriptorPtr                    getModule (int addr) const ;
  coe_driver::ModuleDescriptorPtr                          getModule (int addr);
                                                                                              
  const coe_driver::NetworkDescriptorPtr&                  getNetworkDescriptor () const;
  coe_driver::NetworkDescriptorPtr                         getNetworkDescriptor ();
                                                           
  const std::map<int,std::string>                          getAddressUniqueIdMap()            const { return network_->getAddressUniqueIdMap(); }
                                                                                                                 
  const coe_driver::ModuleDiagnosticPtr&                   getCoeModuleTypedErrors(int addr ) const;
  coe_driver::ModuleDiagnosticPtr                          getCoeModuleTypedErrors(int addr );
  
  const std::map< int, coe_driver::ModuleDiagnosticPtr >&  getCoeDriverTypedErrors( )         const { return module_diagnostic_; }
  std::map< int, coe_driver::ModuleDiagnosticPtr >&        getCoeDriverTypedErrors( )               { return module_diagnostic_; }

  const coe_driver::MasterDiagnosticPtr&                   getCoeDriverGenericErrors( )       const { return master_diagnostic_; }
  coe_driver::MasterDiagnosticPtr                          getCoeDriverGenericErrors( )             { return master_diagnostic_; }

   
  bool resetErrors()
  {
    for( auto & e : module_diagnostic_ )
      e.second->clear();
    
    master_diagnostic_->clear();
    
    return true;
  }
};






/**
 * 
 * inline section
 */
inline
const std::vector<coe_driver::ModuleDescriptorPtr>& CoeMaster::getModules() const 
{ 
  return modules_; 
}
inline
std::vector<coe_driver::ModuleDescriptorPtr>& CoeMaster::getModules()
{ 
  return modules_; 
}
inline
const coe_driver::NetworkDescriptorPtr&  CoeMaster::getNetworkDescriptor () const 
{ 
  return network_; 
}
inline
coe_driver::NetworkDescriptorPtr  CoeMaster::getNetworkDescriptor () 
{ 
  return network_; 
}                                                   
inline
const coe_driver::ModuleDescriptorPtr CoeMaster::getModule(int addr) const 
{ 
  auto const it = std::find_if( modules_.begin(), modules_.end(), [&addr]( const coe_driver::ModuleDescriptorPtr m ) { return m->getAddress() == addr; }  );
  if( it == modules_.end() ) 
    throw std::runtime_error(std::string("address " + std::to_string(addr) + "not mapped" ).c_str());
  return *it;
}
inline
coe_driver::ModuleDescriptorPtr CoeMaster::getModule(int addr)
{ 
  auto it = std::find_if( modules_.begin(), modules_.end(), [&addr]( coe_driver::ModuleDescriptorPtr m ) { return m->getAddress() == addr; }  );
  if( it == modules_.end() ) 
    throw std::runtime_error(std::string("address " + std::to_string(addr) + "not mapped" ).c_str());
  return *it;
}
inline
const coe_driver::ModuleDiagnosticPtr& CoeMaster::getCoeModuleTypedErrors(int address ) const
{ 
  auto it = module_diagnostic_.find( address );
  if( it != module_diagnostic_.end() )
    return module_diagnostic_.at(address); 
  
  throw std::runtime_error("Address not mapped");
}
inline
coe_driver::ModuleDiagnosticPtr CoeMaster::getCoeModuleTypedErrors(int address ) 
{ 
  auto it = module_diagnostic_.find( address );
  if( it != module_diagnostic_.end() )
    return module_diagnostic_.at(address); 
  
  throw std::runtime_error("Address not mapped");
}
  

inline 
std::string CoeMaster::getID()
{
  std::string id = "CoE Master";
  if (id == std::string("H0000000"))
    return "unknown";
  return id;
}



#endif



